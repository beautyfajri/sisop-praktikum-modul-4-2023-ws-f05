#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdlib.h>
#include <libgen.h>
#include <stdbool.h>
#include <sys/wait.h>
void process_file(char* path);
void process_directory(char* dir);

void encodeFile(const char* filePath) {
    char command[1024];
    snprintf(command, sizeof(command), "base64 -w 0 \"%s\" > \"%s.b64\"", filePath, filePath);
    int result = system(command);
    if (result != 0) {
        fprintf(stderr, "Error encoding file: %s\n", filePath);
        return;
    }
    result = remove(filePath);
    if (result != 0) {
        fprintf(stderr, "Error removing original file: %s\n", filePath);
        return;
    }
    char encodedFilePath[1024];
    snprintf(encodedFilePath, sizeof(encodedFilePath), "%s.b64", filePath);
    result = rename(encodedFilePath, filePath);
    if (result != 0) {
        fprintf(stderr, "Error renaming encoded file: %s\n", encodedFilePath);
        return;
    }
    chmod(filePath, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}

void decodeFile(const char* filePath) {
    char command[1024];
    snprintf(command, sizeof(command), "base64 -d \"%s\" > \"%s.decoded\"", filePath, filePath);
    int result = system(command);
    if (result != 0) {
        fprintf(stderr, "Error decoding file: %s\n", filePath);
        return;
    }
    result = remove(filePath);
    if (result != 0) {
        fprintf(stderr, "Error removing original file: %s\n", filePath);
        return;
    }
    char decodedFilePath[1024];
    snprintf(decodedFilePath, sizeof(decodedFilePath), "%s.decoded", filePath);
    result = rename(decodedFilePath, filePath);
    if (result != 0) {
        fprintf(stderr, "Error renaming decoded file: %s\n", decodedFilePath);
        return;
    }
    chmod(filePath, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}

bool isEncoded(const char* filePath) {
    FILE* file = fopen(filePath, "rb");
    if (!file) {
        fprintf(stderr, "Error opening file: %s\n", filePath);
        return false;
    }
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    rewind(file);
    char* buffer = (char*)malloc(fileSize + 1);
    if (!buffer) {
        fclose(file);
        fprintf(stderr, "Error allocating memory\n");
        return false;
    }
    if (fread(buffer, 1, fileSize, file) != fileSize) {
        fclose(file);
        free(buffer);
        fprintf(stderr, "Error reading file: %s\n", filePath);
        return false;
    }
    fclose(file);
    buffer[fileSize] = '\0';
    size_t length = strlen(buffer);
    if (length % 4 != 0) {
        free(buffer);
        return false;
    }
    bool valid = true;
    for (size_t i = 0; i < length; i++) {
        char c = buffer[i];
        if (!isalnum(c) && c != '+' && c != '/' && c != '=' && !isspace(c)) {
            valid = false;
            break;
        }
    }
    free(buffer);
    return valid;
}

bool isBinary(const char *str) {
    while (*str != '\0') {
        if (*str != '0' && *str != '1' && *str != ' ') {
            return false;
        }
        str++;
    }
    return true;
}

char* getLetters(const char* path) {
    int length = strlen(path);
    char* letters = (char*)malloc((length + 2) * sizeof(char));
    int index = 0;
    if (letters == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return NULL;
    }

    for (int i = 1; i < length; i++) {
        if (path[i] == '/') {
            letters[index++] = path[i + 1];
        }
    }
    letters[index] = '\0'; 
    return letters;
}

bool containLetters(const char* str) {
    while (*str != '\0') {
        char c = tolower(*str);
        if (c == 'l' || c == 'u' || c == 't' || c == 'h') {
            return true;
        }
        str++;
    }
    return false;
}

int countLettersFolder(const char* str) {
    int count = 0;
    while (*str) {
        if ((*str >= 'a' && *str <= 'z') || (*str >= 'A' && *str <= 'Z')) {
            count++;
        }
        str++;
    }
    return count;
}

int countLetters(const char* str) {
    int count = 0;
    const char* dot = strrchr(str, '.');
    size_t nameLength = (dot != NULL) ? (size_t)(dot - str) : strlen(str);
    for (size_t i = 0; i < nameLength; i++) {
        if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z')) {
            count++;
        }
    }   
    return count;
}

char* toBinary(const char* str) {
    size_t len = strlen(str);
    char* binary = (char*)malloc((9 * len + 1) * sizeof(char));
    size_t index = 0;    
    for (size_t i = 0; i < len; ++i) {
        unsigned char val = (unsigned char)str[i];
        for (int j = 7; j >= 0; --j) {
            binary[index++] = (val & (1 << j)) ? '1' : '0';
        }
 	if (i < len-1)binary[index++] = ' ';
    }
    binary[index] = '\0';
    return binary;
}

char* toUpper(const char* str) {
    size_t length = strlen(str);
    char* str2 = malloc((length + 1) * sizeof(char));
    if (str2 == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return NULL;
    }
    for (size_t i = 0; i < length; i++) {
        str2[i] = toupper(str[i]); 
    }
    str2[length] = '\0'; 
    return str2;  
}

char* toLower(const char* str) {
    size_t length = strlen(str);
    char* str2 = malloc((length + 1) * sizeof(char));
    if (str2 == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return NULL;
    }
    for (size_t i = 0; i < length; i++) {
        str2[i] = tolower(str[i]); 
    }
    str2[length] = '\0'; 
    return str2; 
}


void process_file(char* path) {
    char* filename = basename(path);
    char* mutable_newname;
    if (isBinary(filename)){
	mutable_newname = filename;    
    } else if (countLetters(filename) <= 4) {
        mutable_newname = toBinary(filename);
    } else {
        mutable_newname = toLower(filename);
    }    
    char fullpath[PATH_MAX + 8];
    strncpy(fullpath, path, PATH_MAX + 8);
    char* dirpath = dirname(fullpath);
    char full_newpath[PATH_MAX + NAME_MAX + 10];
    snprintf(full_newpath, sizeof(full_newpath), "%s/%s", dirpath, mutable_newname); 
    int result = rename(path, full_newpath);
    if (result == 0) {
        printf("File renamed successfully.\n");
    } else {
        printf("Error renaming file.\n");
    }
    free(mutable_newname);
    chmod(full_newpath, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if(containLetters(getLetters(full_newpath)) && !isEncoded(full_newpath))encodeFile(full_newpath);
    else if (!containLetters(getLetters(full_newpath)) && isEncoded(full_newpath))decodeFile(full_newpath);
}


void process_directory(char* dir) {
    DIR* dire;
    struct dirent* entry;
    dire = opendir(dir);
    if (dire == NULL) {
        fprintf(stderr, "Error opening directory: %s\n", dir);
        return;
    }
    while ((entry = readdir(dire)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        char path[PATH_MAX + 8];
        snprintf(path, sizeof(path), "%s/%s", dir, entry->d_name);
        if (entry->d_type == DT_DIR) {
            char* folname = basename(path);
            char* mutable_newname;
	    if (isBinary(folname)){
		mutable_newname = folname;
            } else if (countLettersFolder(folname) <= 4) {
                mutable_newname = toBinary(folname);
            } else {
                mutable_newname = toUpper(folname);
            }
            char fullpath[PATH_MAX + 8];
            strncpy(fullpath, path, PATH_MAX + 8);
            char* dirpath = dirname(fullpath);
            char full_newpath[PATH_MAX + NAME_MAX + 10];
            snprintf(full_newpath, sizeof(full_newpath), "%s/%s", dirpath, mutable_newname);
            int result = rename(path, full_newpath);
            if (result != 0) {
                fprintf(stderr, "Error renaming directory: %s\n", path);
            }
            free(mutable_newname);
	    process_directory(full_newpath);
        } else {
            process_file(path);
        }
    }
    closedir(dire); 
}

char *passwords = NULL;
char *source_path = "./inifolderetc/sisop";
static const char *dirpath = NULL;

static int xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);
    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;
    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;
    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;
    char password[100];
    int attempts = 3;
    while (attempts > 0)
    {
        printf("Enter password: ");
        fgets(password, sizeof(password), stdin);
        password[strcspn(password, "\n")] = '\0';
        if (strcmp(password, passwords) == 0)
            break;

        attempts--;
        printf("Invalid password. \n");
    }

    if (attempts == 0)
    {
        printf("Too many failed attempts. Access denied.\n");
        close(fd);
        return -EACCES; 
    }
    system("clear");
    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}


static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    char* folname;
    const char* path_again = malloc(strlen(path) + 1);  
    strcpy((char*)path_again, path);
    const char* mutable_newname = dirname((char*)path_again);
    if (isBinary(basename((char*)path))){
	folname = basename((char*)path);
    } else if (countLettersFolder(basename((char*)path)) <= 4) {
        folname = toBinary(basename((char*)path));
    } else {
        folname = toUpper(basename((char*)path));
    } 
    snprintf(fpath, sizeof(fpath), "%s%s/%s", dirpath, mutable_newname, folname);
    
    mkdir(fpath, mode);
    free(folname);free((void*)path_again);
    
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd;

    (void)fi;

    fd = open(fpath, O_WRONLY);

    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

int xmp_rename(const char *from, const char *to)
{
    char src[1000];
    char dst[1000];
    sprintf(src, "%s%s", dirpath, from);
    sprintf(dst, "%s%s", dirpath, to);

    struct stat st;
    if (stat(src, &st) == -1) {
        return -errno;
    }

    int res;
    res = rename(src, dst);
        if (res == -1) {
            return -errno;
        }
    if (S_ISDIR(st.st_mode)) {
        

        process_directory(dst);  
    } else {
        process_file(dst);
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .write = xmp_write, 
    .rename = xmp_rename
};

char *source_normpath = "./normetc/sisop";
static const char* normpath = NULL;

static int norm_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fullpath[PATH_MAX];
    snprintf(fullpath, sizeof(fullpath), "%s%s", normpath, path);
    res = lstat(fullpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int norm_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                        off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char fullpath[PATH_MAX];
    snprintf(fullpath, sizeof(fullpath), "%s%s", normpath, path);
    dp = opendir(fullpath);
    if (dp == NULL)
        return -errno;
    while ((de = readdir(dp)) != NULL) {
        if (filler(buf, de->d_name, NULL, 0) != 0)
            break;
    }
    closedir(dp);

    return 0;
}

static int norm_read(const char *path, char *buf, size_t size, off_t offset,
                     struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fullpath[PATH_MAX];
    snprintf(fullpath, sizeof(fullpath), "%s%s", normpath, path);
    fd = open(fullpath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);

    return res;
}

static int norm_mkdir(const char *path, mode_t mode)
{
    int res;
    char fullpath[PATH_MAX];
    snprintf(fullpath, sizeof(fullpath), "%s%s", normpath, path);
    res = mkdir(fullpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int norm_write(const char *path, const char *buf, size_t size,
                      off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fullpath[PATH_MAX];
    snprintf(fullpath, sizeof(fullpath), "%s%s", normpath, path);
    fd = open(fullpath, O_WRONLY);
    if (fd == -1)
        return -errno;
    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);

    return res;
}

static int norm_rename(const char *oldpath, const char *newpath)
{
    int res;
    char full_oldpath[PATH_MAX];
    snprintf(full_oldpath, sizeof(full_oldpath), "%s%s", normpath, oldpath);

    char full_newpath[PATH_MAX];
    snprintf(full_newpath, sizeof(full_newpath), "%s%s", normpath, newpath);

    res = rename(full_oldpath, full_newpath);
    if (res == -1)
        return -errno;

    return 0;
}

static struct fuse_operations norm_oper = {
    .getattr = norm_getattr,
    .readdir = norm_readdir,
    .read = norm_read,
    .mkdir = norm_mkdir,
    .write = norm_write,
    .rename = norm_rename
};

void getPassword(){
    passwords = malloc(sizeof(char) * 100);
    printf("Enter password for file: ");
    fgets(passwords, 100, stdin);
    passwords[strcspn(passwords, "\n")] = '\0';
    system("clear");
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        printf("Usage: %s -f <modif_mountpoint> <norm_mountpoint>\n", argv[0]);
        return 1;
    }
    char* modif_mount = argv[2];
    char* norm_mount = argv[3];
    system("wget -O inifolderetc.zip \"drive.google.com/u/3/uc?id=1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT&export=download&confirm=yes\" && unzip inifolderetc.zip");
    system("cp -R inifolderetc/ normetc/");
    process_directory(source_path);
    dirpath = realpath(source_path, NULL);
    normpath = realpath(source_normpath, NULL);
    getPassword();
    char cmd [100];
    sprintf(cmd, "mkdir %s %s", modif_mount, norm_mount);
    system(cmd);
    umask(0);
    pid_t pid = fork();
    if (pid == 0) {
    int xmp_argc = 3;
    char* xmp_argv[] = { argv[0], "-f", modif_mount };
    int xmp_ret = fuse_main(xmp_argc, xmp_argv, &xmp_oper, NULL);
    exit(xmp_ret);
    }
    else {
    int norm_argc = 3;
    char* norm_argv[] = { argv[0], "-f", norm_mount};
    int norm_ret = fuse_main(norm_argc, norm_argv, &norm_oper, NULL);
    wait(NULL);
    exit(norm_ret);
    }
    return 0;
}




