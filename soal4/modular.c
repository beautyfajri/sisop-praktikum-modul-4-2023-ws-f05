#define FUSE_USE_VERSION 28
#include <stdlib.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <pwd.h>

#define SPLIT_SIZE 1024

FILE *logFile;

void logCall(int argc, char *argv[]) {
  time_t currentTime = time(NULL);
  struct tm *localTime = localtime(&currentTime);

  char cmd[1000];
  char level[10];
  sprintf(cmd, "%s", argv[0]);

  if (!strcmp(cmd,"RMDIR") || !strcmp(cmd,"UNLINK")) {
    sprintf(level,"FLAG");
  }
  else {
    sprintf(level,"REPORT");
  }


  for (int i = 1; i < argc; i++) {
    sprintf(cmd + strlen(cmd), "::%s",argv[i]);
  }

  int second = localTime->tm_sec;
  int minute = localTime->tm_min;
  int hour = localTime->tm_hour;
  int day = localTime->tm_mday;
  int month = localTime->tm_mon + 1;
  int year = localTime->tm_year % 100;
  fprintf(logFile, "%s::%d%d%d-%02d:%02d:%02d::%s\n", level, day, month, year, hour, minute, second, cmd);
  fflush(logFile);
}


const char *getSplitFilePath(const char *path, int index) {
  char *splitFilePath = malloc(strlen(path) + 5);
  sprintf(splitFilePath, "%s.%03d", path, index);
  return splitFilePath;
}

char *isModularDir(const char *path) {
  char temp[strlen(path) + 1];
  strcpy(temp, path);
  const char *prefix = "module_";
  int prefixLen = 7;

  const char* delim = "/";
  char *token = strtok((char *) temp, delim);

  while (token != NULL) {
    if (strncmp(token, prefix, prefixLen) == 0) {
      int len = strstr(path, token) - path + strlen(token);
      char *substring = (char *) malloc(len + 1);
      strncpy(substring, path, len);
      substring[len] = '\0';
      return substring;
    }
    token = strtok(NULL, delim);
  }
  return NULL;
}

int isSplitFile(const char *path) {
  const char *suffix = strrchr(path, '.');

  if (suffix == NULL) return 0;
  for (int i = 0; i <= 999; i++) {
    char index[6];
    sprintf(index, ".%.03d", i);
    if (!strcmp(suffix, index)) return 1;
  }
  return 0;
}

void splitFile(const char *path) {
  FILE *fin = fopen(path, "rb");
  if (fin == NULL) return;

  fseek(fin, 0, SEEK_END);
  long fileSize = ftell(fin);
  rewind(fin);

  int numOfFiles = (fileSize + SPLIT_SIZE - 1) / SPLIT_SIZE;

  char output[4096];
  FILE *fout;
  size_t bytesRead;
  char buffer[SPLIT_SIZE];

  for (int i = 0; i < numOfFiles; i++) {
    sprintf(output, "%s.%03d", path, i);
    fout = fopen(output, "wb");
    if (fout == NULL) {
      fclose(fin);
      return;
    }

    bytesRead = fread(buffer, 1, SPLIT_SIZE, fin);

    fwrite(buffer, 1, bytesRead, fout);

    fclose(fout);
  }

  fclose(fin);
  remove(path);
}

void combineFiles(const char *path) {
  FILE *fout = fopen(path, "wb");
  if (fout == NULL) {
    perror("fopen");
    return;
  }

  int index = 0;
  const char *splitFilePath = getSplitFilePath(path, index++);
  while (access(splitFilePath, F_OK) == 0) {
    FILE *fs = fopen(splitFilePath, "rb");
    if (fs == NULL) {
      fclose(fout);
      perror("fopen");
      return;
    }
    
    char buffer[SPLIT_SIZE];
    size_t bytesRead = fread(buffer, 1, SPLIT_SIZE, fs);

    fwrite(buffer, 1, bytesRead, fout);
    
    fclose(fs);

    splitFilePath = getSplitFilePath(path, index++);
  }
  index = 0;
  splitFilePath = getSplitFilePath(path, index++);
  while (access(splitFilePath, F_OK) == 0) {
    remove(splitFilePath);
    splitFilePath = getSplitFilePath(path, index++);
  }

  fclose(fout);
}

void demodularize(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      demodularize(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      int len = strlen(filePath);
      if (len >= 4 && !strcmp(filePath + len - 4, ".000")) {
        char baseName[256];
        strncpy(baseName, filePath, len - 4);
        combineFiles(baseName);
      }
    }
  }
}

void modularize(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

      logCall(1, (char*[1]) {filePath});
    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      modularize(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      splitFile(filePath);
    }
  }
}

static int op_getattr(const char *path, struct stat *stbuf) {
  int result = lstat(path,stbuf);
  if (result == -1) return -errno;

  char *argv[] = {"GETATTR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_access(const char *path, int mask) {
  char *argv[] = {"ACCESS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_readlink(const char *path, char *buf, size_t size) {
  char *argv[] = {" LINK", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
  DIR *dp;
  struct dirent *de;
  (void) offset;
  (void) fi;

  dp = opendir(path);

  if (dp == NULL) return -errno;

  while ((de = readdir(dp)) != NULL) {
    size_t len = strlen(de->d_name);
    if (isSplitFile(de->d_name)) {
      if (len >= 4 && !strcmp(de->d_name + len - 4, ".000")) {
        char baseName[256];
        char filePath[4096];
        strncpy(baseName, de->d_name, len - 4);
        baseName[len - 4] = '\0';
        sprintf(filePath, "%s/%s", path, baseName);
        combineFiles(filePath);
      }
    }
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (filler(buf, de->d_name, &st, 0)) break;
  }
  closedir(dp);

  char *argv[] = {"READDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc,argv);

  return 0;
}

static int op_mknod(const char *path, mode_t mode, dev_t rdev) {
  char *argv[] = {"MKNOD", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);
  return 0;
}

static int op_symlink(const char *to, const char *from) {
  char *argv[] = {"SYMLINK", (char *) to, (char *) from};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_mkdir(const char *path, mode_t mode) {
  int result = mkdir(path, mode);
  if (result == -1) return -errno;

  char *argv[] = {"MKDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_unlink(const char *path) {
  int result = unlink(path);
  if (result == -1) return -errno;

  char *argv[] = {"UNLINK", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);
  return 0;
}

static int op_rmdir(const char *path) {
  int result = rmdir(path);
  if (result == -1) return -errno;

  char *argv[] = {"RMDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_rename(const char *oldpath, const char *newpath) {
  int result = rename(oldpath, newpath);
  if (result == -1) return -errno;

  char *argv[] = {"RENAME", (char *) oldpath, (char *) newpath};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  char *path = isModularDir(newpath);
  if (path) {
    modularize(newpath);
  }
   else {
    demodularize(newpath);
  }

  return 0;
}
static int op_link(const char *to, const char *from) {
  char *argv[] = {"LINK", (char *) to, (char *) from};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_chmod(const char *path, mode_t mode) {
  char *argv[] = {"CHMOD", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_chown(const char *path, uid_t uid, gid_t gid) {
  char *argv[] = {"CHOWN", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_truncate(const char *path, off_t size) {
  char *argv[] = {"TRUNCATE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_utimens(const char *path, const struct timespec ts[2]) {
  char *argv[] = {"UTIMENS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_open(const char *path, struct fuse_file_info *fi) {
  char *argv[] = {"OPEN", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  (void)fi;

  FILE* fp = fopen(path, "r");
  if (fp == NULL) {
    return -errno;
  }
  if (fseek(fp, offset, SEEK_SET) == -1) {
    fclose(fp);
    return -errno;
  }

  size_t bytesRead = fread(buf, 1, size, fp);

  fclose(fp);
  
  char *argv[] = {"READ", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return bytesRead;
}

static int op_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  (void)fi;

  FILE *fp = fopen(path, "w+");
  if (fp == NULL) {
    return -errno;
  }

  if (fseek(fp, offset, SEEK_SET) == -1) {
    fclose(fp);
    return -errno;
  }

  if (fwrite(buf, size, 1, fp) != 1) {
    fclose(fp);
    return -errno;
  }

  fclose(fp);

  char *argv[] = {"WRITE", (char *) path, (char *) buf};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return size;
}


static int op_statfs(const char *path, struct statvfs *stbuf) {
  char *argv[] = {"STATFS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
  if (access(path, F_OK) == 0) return -EEXIST;

  int result = creat(path, mode);
  if ( result == -1) return -errno;

  fi->fh = result;

  char *argv[] = {"CREATE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_release(const char *path, struct fuse_file_info *fi) {
  char *argv[] = {"RELEASE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_fsync(const char *path, int isdatasync, struct fuse_file_info* fi) {
  char *argv[] = {"FSYNC", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_setxattr(const char* path, const char* name, const char* value, size_t size, int flags) {
  char *argv[] = {"SETXATTR", (char *) path, (char *) name, (char *) value};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_getxattr(const char* path, const char* name, char* value, size_t size) {
  char *argv[] = {"GETXATTR", (char *) path, (char *) name, (char *) value};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_listxattr(const char* path, char* list, size_t size) {
  char *argv[] = {"LISTXATTR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static int op_removexattr(const char *path, const char *name) {
  char *argv[] = {"REMOVEXATTR", (char *) path, (char *) name};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}

static struct fuse_operations xmp_oper = {
  .getattr = op_getattr,
  .access = op_access,
  .readlink = op_readlink,
  .readdir = op_readdir,
  .mknod = op_mknod,
  .mkdir = op_mkdir,
  .symlink = op_symlink,
  .unlink = op_unlink,
  .rmdir = op_rmdir,
  .rename = op_rename,
  .link = op_link,
  .chmod = op_chmod,
  .chown = op_chown,
  .truncate = op_truncate,
  .utimens = op_utimens,
  .open = op_open,
  .read = op_read,
  .write = op_write,
  .statfs = op_statfs,
  .create = op_create,
  .release = op_release,
  .fsync = op_fsync,
  .setxattr = op_setxattr,
  .getxattr = op_getxattr,
  .listxattr = op_listxattr,
  .removexattr = op_removexattr,
};


int main(int  argc, char *argv[]) {
  umask(0);

  char *homeDir = getpwuid(getuid())->pw_dir;

  char logFilePath[256];
  sprintf(logFilePath, "%s/fs_module.log", homeDir);
  logFile = fopen(logFilePath, "w");

  if (logFile == NULL) {
    perror("bruh");
    return 1;
  }

  return fuse_main(argc, argv, &xmp_oper, NULL);
}