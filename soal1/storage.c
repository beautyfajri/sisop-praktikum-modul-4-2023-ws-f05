#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 1000

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoUrl[100];
    // Tambahkan data lainnya sesuai kebutuhan
} Player;

int main() {
    // Menjalankan perintah untuk mendownload dataset menggunakan Kaggle API
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("rm fifa-player-stats-database.zip");

    const char *filename = "FIFA23_official_data.csv";
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found.\n");
        return 1;
    }

    // Filter data
    char command[MAX_LINE_LENGTH];
    sprintf(command, "awk -F',' 'BEGIN { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", \"ID\", \"Name\", \"Age\", \"Photo\", \"Nationality\", \"Flag\", \"Overall\", \"Potential\", \"Club\", \"Club Logo\", \"Value\", \"Wage\", \"Special\", \"Preferred Foot\", \"Int. Reputation\", \"Weak Foot\", \"Skill Moves\", \"Work Rate\", \"Body Type\", \"Real Face\", \"Position\", \"Joined\", \"Loaned From\", \"Contract Until\", \"Height\", \"Weight\", \"Release Clause\", \"Kit Number\", \"Best Overall Rating\" } NR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28 }' %s", filename);
    system(command);

    fclose(file);

    return 0;
}


