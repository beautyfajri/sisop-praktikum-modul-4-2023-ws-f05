# sisop-praktikum-modul-4-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## NOMOR 1
#### P......

```shell
define MAX_PLAYERS 1000
define MAX_LINE_LENGTH 1000

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoUrl[100];
} Player;
```
Potongan kode yang diberikan merupakan definisi beberapa preprocessor directives (#define) `define MAX_PLAYERS 1000:` Preprocessor directive ini mendefinisikan sebuah konstanta bernama MAX_PLAYERS dengan nilai 1000. Konstanta ini kemungkinan digunakan untuk membatasi jumlah maksimum pemain yang dapat diolah atau disimpan dalam program dan `define MAX_LINE_LENGTH 1000:` Preprocessor directive ini mendefinisikan sebuah konstanta bernama MAX_LINE_LENGTH dengan nilai 1000. Konstanta ini kemungkinan digunakan untuk membatasi panjang maksimum dari sebuah baris teks atau string yang diolah dalam program.

Untuk `char name[100]:` yakni array karakter dengan ukuran 100 yang menyimpan nama pemain. `char club[100]:` yakni array karakter dengan ukuran 100 yang menyimpan klub pemain.`int age:` merupakan variabel bertipe integer yang menyimpan umur pemain dan `int potential:` merupakan variabel bertipe integer yang menyimpan potensi pemain. serta `char photoUrl[100]:` adalah array karakter dengan ukuran 100 yang menyimpan URL foto pemain.

```shell
int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("rm fifa-player-stats-database.zip");
```
Potongan kode tersebut merupakan implementasi dari kode `system("kaggle datasets download -d bryanb/fifa-player-stats-database");` dengan menggunakan fungsi `system()` untuk menjalankan perintah kaggle datasets download `-d bryanb/fifa-player-stats-database`. Perintah ini digunakan untuk mengunduh dataset `"fifa-player-stats-database"` dari platform Kaggle. Untuk `system("unzip fifa-player-stats-database.zip");` digunakan untuk menjalankan perintah unzip `fifa-player-stats-database.zip`. Perintah ini digunakan untuk mengekstrak file `ZIP` yang telah diunduh sebelumnya. Sedangkan `system("rm fifa-player-stats-database.zip");` untuk menjalankan perintah `rm fifa-player-stats-database.zip`. Perintah ini digunakan untuk menghapus file ZIP setelah diekstrak.

```shell
 const char *filename = "FIFA23_official_data.csv";
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found.\n");
        return 1;
    }
```
Untuk `const char *filename = "FIFA23_official_data.csv";` mendeklarasikan variabel filename sebagai pointer ke konstanta string `"FIFA23_official_data.csv"`. Variabel ini akan digunakan sebagai nama file yang akan dibuka. Untuk `FILE *file = fopen(filename, "r");` mendeklarasikan pointer file bertipe FILE dan menginisialisasinya dengan hasil dari pemanggilan fungsi `fopen()` untuk membuka file dengan mode `"r"`. File yang akan dibuka adalah file dengan nama yang disimpan dalam variabel filename.

`if (file == NULL)` maka kode tersebut akan melakukan pengecekan apakah pembukaan file berhasil atau tidak. Jika file bernilai `NULL`, artinya file tidak ditemukan atau terjadi kesalahan dalam membukanya. Pada kasus ini, pesan `"File not found."` akan dicetak dan fungsi `main()` akan mengembalikan nilai 1 untuk menandakan adanya kesalahan.

```shell
    char command[MAX_LINE_LENGTH];
    sprintf(command, "awk -F',' 'BEGIN { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", \"ID\", \"Name\", \"Age\", \"Photo\", \"Nationality\", \"Flag\", \"Overall\", \"Potential\", \"Club\", \"Club Logo\", \"Value\", \"Wage\", \"Special\", \"Preferred Foot\", \"Int. Reputation\", \"Weak Foot\", \"Skill Moves\", \"Work Rate\", \"Body Type\", \"Real Face\", \"Position\", \"Joined\", \"Loaned From\", \"Contract Until\", \"Height\", \"Weight\", \"Release Clause\", \"Kit Number\", \"Best Overall Rating\" } NR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28 }' %s", filename);
    system(command);

    fclose(file);

    return 0;
}
```
Untuk `char command[MAX_LINE_LENGTH];` mendeklarasikan array karakter command dengan ukuran `MAX_LINE_LENGTH`. Array ini akan digunakan untuk menyimpan perintah yang akan dijalankan menggunakan fungsi `system()`.`sprintf(command, "awk -F',' 'BEGIN { ... }' %s", filename);` digunakan untuk memformat string perintah yang akan dieksekusi. Pada kasus ini, perintah `awk` akan mencetak baris-baris data yang memenuhi kriteria tertentu ke dalam format yang ditentukan. `%s` akan digantikan dengan nama file yang disimpan dalam variabel filename.

